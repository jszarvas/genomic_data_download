#!/usr/bin/env python3.6

import os
import sys
import argparse
import json
import sqlite3
import requests
import shlex
import subprocess
import multiprocessing
import glob
import time

def timing(t0, message):
    t1 = time.time()
    print("{0} Time used: {1} seconds".format(message, int(t1-t0)), file=sys.stdout)
    return

def call_cmd(cmd):
    p = subprocess.run(shlex.split(cmd))
    return p.returncode

def get_subfolder(data_parent_dir):
    """Avoid having more than 10000 files in one subfolder"""
    olddir = os.getcwd()
    os.chdir(data_parent_dir)
    ptrn = "data-[0-9]*"
    dts = glob.glob(ptrn)
    os.chdir(olddir)
    # no subfolder exist yet
    if not dts:
        subfolder = os.path.join(data_parent_dir, "data-{:04d}".format(1))
    else:
        dts.sort()
        subfolder = os.path.join(data_parent_dir, dts[-1])
        if len(os.listdir(subfolder)) > 10000:
            # create the next one
            i = int(dts[-1].split("-")[-1]) + 1
            subfolder = os.path.join(data_parent_dir, "data-{:04d}".format(i))
    if not os.path.exists(subfolder):
        os.mkdir(subfolder)
    return subfolder

def minimum_metadata(ena_record, enforce=True):
    """Require both collection date and location"""
    if enforce and (not ena_record["collection_date"] or not ena_record["country"]):
        return False
    return True

def download_loop(record):
    """Loop around wget command"""
    commands = record[0]
    for cmd in commands:
        tries = 3
        ex = 1
        while ex and tries > 0:
            ex = call_cmd(cmd)
            if ex:
                tries -= 1
                time.sleep(5)
        if ex:
            return None
    return record[1:]

parser = argparse.ArgumentParser(
    description='Prepares ENA samples for processing')
parser.add_argument(
    '-u',
    dest="query_url",
    default="https://www.ebi.ac.uk/ena/portal/api/search?dataPortal=ena&query=tax_tree(2697049)&result=sequence&fields=collection_date,country,description&format=json",
    help='Url for Portal API search')
parser.add_argument(
    '-o',
    dest="data_folder",
    help='Path to data folder for fasta')
parser.add_argument(
    '-d',
    dest="database",
    help="Sample database"
)
parser.add_argument(
    '-p',
    dest="parallel",
    type=int,
    default=2,
    help="Number of parallel processes for download"
)
parser.add_argument(
    '-l',
    dest="limit",
    type=int,
    default=0,
    help="Limit number of samples added"
)
parser.add_argument(
    '-s',
    dest="save_cycle",
    type=int,
    default=20,
    help="Save to database every X sequences"
)
parser.add_argument(
    '-m',
    dest="loose_metadata",
    action="store_true",
    help="Download records without minimum metadata"
)
args = parser.parse_args()

# init timer
t0 = time.time()

#start up the db during first run_id
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS samples
    (accession TEXT PRIMARY KEY,
    path TEXT,
    feature_path TEXT,
    dl_date TEXT DEFAULT CURRENT_DATE,
    included INTEGER DEFAULT NULL);''')
cur.execute('''CREATE TABLE IF NOT EXISTS metadata
    (accession TEXT PRIMARY KEY,
    description TEXT,
    collection_date TEXT,
    country TEXT);''')
conn.commit()

FASTA_BASE = "wget --quiet --timeout=30 --tries=3 --waitretry=2 -O {0} https://www.ebi.ac.uk/ena/browser/api/fasta/{1}?download=True"
FASTQ_BASE = "wget --quiet --timeout=30 --tries=3 --waitretry=2 --continue --directory-prefix={0} ftp://{1}"

data_subfolder = get_subfolder(args.data_folder)

strictness = True
if args.loose_metadata:
    strictness = False

# make the GET search Portal API
extracted_record_path = None
entries = []
valid_items = 0
accession_type = "accession"
accession_levels = {"sequence": "accession",
                    "read_run": "run_accession",
                    "read_experiment": "experiment_accession"}
# if other accession type is returned, use that one
for result_type in ["read_run", "sequence"]:
    if args.query_url.find(result_type) != -1:
        accession_type = accession_levels[result_type]
response = requests.get(args.query_url)
if response.status_code == 200:
    for item in response.json():
        cur.execute('''SELECT * FROM samples WHERE accession=?''', (item[accession_type],))
        if cur.fetchone() is None and minimum_metadata(item, strictness):
            unit = []
            wget_cmd = []
            record_path = None
            if "fastq_ftp" in item.keys():
                # raw reads
                tmp = []
                raw_files = item['fastq_ftp'].split(";")
                if len(raw_files) == 3:
                    # throw away the single reads
                    raw_files = raw_files[1:]
                for raw_read in raw_files:
                    fastq_path = os.path.join(data_subfolder, os.path.basename(raw_read))
                    tmp.append(fastq_path)
                    wget_cmd.append(FASTQ_BASE.format(data_subfolder, raw_read))
                record_path = ",".join(tmp)
            else:
                record_path = os.path.join(data_subfolder, "{}.fasta".format(item[accession_type]))
                wget_cmd.append(FASTA_BASE.format(record_path, item[accession_type]))

            # construct download unit
            unit.append(wget_cmd)
            # sample_id    pathtoread1,pathtoread2
            unit.append((item[accession_type], record_path, extracted_record_path))
            unit.append((item[accession_type],item["description"], item["collection_date"], item["country"]))
            entries.append(unit)
            valid_items += 1

            if valid_items and valid_items % args.save_cycle == 0:
                if __name__ == '__main__':

                    sample_insert = []
                    metadata_insert = []
                    pool = multiprocessing.Pool(args.parallel)
                    for response in pool.imap_unordered(download_loop, entries):
                        if response is not None:
                            sample_insert.append(response[0])
                            metadata_insert.append(response[1])
                    pool.close()
                    pool.join()

                if sample_insert:
                    # insert to db
                    cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?);''', sample_insert)
                    conn.commit()

                    cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, collection_date, country) VALUES (?,?,?,?);''', metadata_insert)
                    conn.commit()

                # clean entries
                entries = []

        if args.limit and valid_items == args.limit:
            break

    # download remaining entries (difference between cycle and limit)
    if entries:
        sample_insert = []
        metadata_insert = []
        for unit in entries:
            response = download_loop(unit)
            if response is not None:
                sample_insert.append(response[0])
                metadata_insert.append(response[1])
        if sample_insert:
            # insert to db
            cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?);''', sample_insert)
            conn.commit()

            cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, collection_date, country) VALUES (?,?,?,?);''', metadata_insert)
            conn.commit()
conn.close()

timing(t0, "# {} Samples and metadata saved to database".format(valid_items))
